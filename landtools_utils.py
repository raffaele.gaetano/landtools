#try:
from osgeo import gdal
#except:
#    import gdal
import datetime
import os
import glob

def find_next_date(datelist, date):
    for i,l in enumerate(datelist):
        if l >= date:
            return i
    return None

def find_first_month_day(dates, month=1, day=1):
    start_year = dates[0].year
    test_date = datetime.datetime(start_year,month,day)
    if dates[0] > test_date:
        start_year += 1
        test_date = datetime.datetime(start_year, month, day)
    return find_next_date(dates, test_date)

def get_period_intervals(date_file, md=[1, 1], duration=365):
    dates = []
    with open(date_file) as f:
        for l in f.read().splitlines():
            try:
                dates.append(datetime.datetime.strptime(l, '%Y%m%d'))
            except:
                continue
    delta = dates[-1] - dates[-2]
    periods = []
    s, e = 0, 0
    S = 0
    while s is not None and e is not None:
        s = find_first_month_day(dates, md[0], md[1])
        if s is not None:
            e = find_next_date(dates, dates[s] + datetime.timedelta(days=duration) - delta)
            if e is not None:
                periods.append((S + s, S + e + 1))
                S += s + 1
                dates = dates[s + 1:]
    return periods

def get_mean_expression(periods):
    l = periods[0][1] - periods[0][0]
    expr = []
    for i in range(1, l + 1):
        s = [p[0] + i for p in periods]
        expr.append('(' + '+'.join(['im1b%d' % x for x in s]) + ')/' + str(len(s)))

    return '{' + ';'.join(expr) + '}'

def get_subperiod_expression(datefile, begin_date, end_date, outdatefile):
    raster_band_list = []
    dates_list= []
    bd = datetime.datetime.strptime(begin_date, "%Y%m%d")
    ed = datetime.datetime.strptime(end_date, "%Y%m%d")

    with open(datefile,'r') as f:
        for i,date in enumerate(f):
            dt = datetime.datetime.strptime(date.strip(), "%Y%m%d")
            if dt >= bd and dt <= ed:
                raster_band_list.append(i+1)
                dates_list.append(date)
    exp = "{" + ';'.join(["im1b{}".format(x) for x in raster_band_list]) + "}"

    with open(outdatefile, 'w') as f:
        for date in dates_list:
            f.write(date+'\n')
    return exp

def get_rainfall_subperiod(rf_folder, begin_date, end_date, outdatefile, product="TRMM_3B43_7_precipitation"):
    bd = datetime.datetime.strptime(begin_date, "%Y%m%d")
    ed = datetime.datetime.strptime(end_date, "%Y%m%d")

    rf_dates = os.path.join(rf_folder, 'rainfall_dates.txt')
    rf_list = sorted(glob.glob(os.path.join(rf_folder,'*scrubbed*.nc')))
    if 'shapeMasked' in os.path.basename(rf_list[0]):
        rf_list = ['NETCDF:"' + x + '":{}'.format(product) for x in rf_list]
    with open(rf_dates, 'w') as f:
        [f.write(os.path.basename(r).split('.')[-2]+'\n') for r in rf_list]

    raster_list = []
    dates_list= []
    with open(rf_dates,'r') as f:
        for date, raster in zip(f,rf_list):
            dt = datetime.datetime.strptime(date.strip(), "%Y%m%d")
            if dt >= bd and dt <= ed:
                raster_list.append(raster)
                dates_list.append(date)

    out_rf_list = raster_list
    with open(outdatefile,'w') as f:
        for date in dates_list:
            f.write(date+'\n')

    return out_rf_list

def setNoDataValue(fn, val=None):
    ds = gdal.Open(fn, gdal.GA_Update)
    if val is None:
        ds.GetRasterBand(1).DeleteNoDataValue()
    else:
        ds.GetRasterBand(1).SetNoDataValue(val)
    ds = None

def getNoDataValue(fn):
    ds = gdal.Open(fn)
    ndv = ds.GetRasterBand(1).GetNoDataValue()
    ds = None
    return ndv