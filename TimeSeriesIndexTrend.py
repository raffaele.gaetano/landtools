# -*- coding: utf-8 -*-

"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (QgsProcessing,
                       QgsFeatureSink,
                       QgsRasterLayer,
                       QgsProcessingException,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterFolderDestination,
                       QgsProcessingParameterFile,
                       QgsProcessingParameterString,
                       QgsProcessingParameterNumber,
                       QgsProcessingParameterVectorLayer,
                       QgsProcessingParameterEnum,
                       QgsProcessingParameterFeatureSink)
from qgis import processing
try:
    from osgeo import gdal
except:
    import gdal
import glob
import os
import sys
import calendar
import datetime

def ModisToYYYYMMDD(raster_list, output):
    dates=[]
    for path in raster_list :
        modisDate = path.split('_')[-2][3:]
        year = int(modisDate[:-3])
        day = int(modisDate[-3:])
        month = 1
        while day - calendar.monthrange(year,month)[1] > 0 and month <= 12:
            day = day - calendar.monthrange(year,month)[1]
            month = month + 1
        month= '0'+str(month) if month < 10 else str(month)
        day= '0'+str(day) if day < 10 else str(day)
        dates.append(str(year)+str(month)+str(day))
    dates.sort()
    with open(output,'w') as f:
        for date in dates:
            f.write(date+'\n')

def setNoDataValue(fn, val=0):
    """ setNoDataValue(fn,val = 0)
    Sets a value for nodata pixels in a raster GDAL dataset.

    INPUT   :   fn      -   GDAL dataset file
                val     -   nodata value (def. 0)

    OUTPUT  :   None
    """
    ds = gdal.Open(fn, gdal.GA_Update)
    for i in range(0, ds.RasterCount):
        band = ds.GetRasterBand(i + 1)
        if val is None:
            band.DeleteNoDataValue()
        else:
            band.SetNoDataValue(val)
        band = None
    ds.FlushCache()
    ds = None

def rainfDates(rain_list, output):
    with open(output,'w') as f:
        for r in rain_list :
            # f.write(os.path.basename(r).split('_')[-2]+'\n')
            f.write(os.path.basename(r).split('.')[-2]+'\n')
    return output

def NDVI_subdates_maker(TS_file,datefile,output_folder,begin_date,end_date,context,feedback):
    raster_band_list = []
    dates_list= []
    with open(datefile,'r') as f:
        for i,date in enumerate(f):
            if int(date)>=begin_date and int(date) <=end_date:
                raster_band_list.append(i+1)
                dates_list.append(int(date))
    exp=''
    for i in raster_band_list :
        exp+='im1b'+str(i)+';'
    exp=exp[:-1]
    out = os.path.join(output_folder,'temp',"sub_ndvi_ts.tif")
    BMX_parameters = {'il':[TS_file],'exp':exp, 'out':out, 'outcontext':os.path.join(output_folder,"outcontext.txt")}
    processing.run('otb:BandMathX',BMX_parameters, context=context, feedback=feedback)
    os.remove(os.path.join(output_folder,"outcontext.txt"))
    
    with open(os.path.join(output_folder,'temp',os.path.basename(datefile)),'w') as f:
        for date in dates_list:
            f.write(str(date)+'\n')
    return out, os.path.join(output_folder,'temp',os.path.basename(datefile))

def subdates_maker(TS_files,datefile,output_folder,begin_date,end_date):
    raster_list = []
    dates_list= []
    with open(datefile,'r') as f:
        for date, raster in zip(f,TS_files):
            if int(date)>=begin_date and int(date) <=end_date:
                raster_list.append(raster)
                dates_list.append(int(date))
    TS_files = raster_list
    with open(os.path.join(output_folder,'temp','rf_dates.txt'),'w') as f:
        for date in dates_list:
            f.write(str(date)+'\n')
    return TS_files, os.path.join(output_folder,'temp','rf_dates.txt')
    
class ExampleProcessingAlgorithm(QgsProcessingAlgorithm):
    """
    This is an example algorithm that takes a vector layer and
    creates a new identical one.

    It is meant to be used as an example of how to create your own
    algorithms and explain methods and variables used to do it. An
    algorithm like this will be available in all elements, and there
    is not need for additional work.

    All Processing algorithms should extend the QgsProcessingAlgorithm
    class.
    """

    # Constants used to refer to parameters and outputs. They will be
    # used when calling the algorithm from another algorithm, or when
    # calling from the QGIS console.

    INPUT = 'INPUT'
    INPUTDATE = 'INPUTDATE'
    CLIP = 'CLIP'
    RAINFALL = 'RAINFALL'
    OUTPUT = 'OUTPUT'
    BEGDATE = 'BEGDATE'
    ENDDATE = 'ENDDATE'
    PREFIX = 'PREFIX'
    REDUCE = 'REDUCE'
    CUMDAY = 'CUMDAY'
    CUMMONTH = 'CUMMONTH'
    NBDAYS = 'NBDAYS'
    RAINDAY = 'RAINDAY'
    RAINMONTH = 'RAINMONTH'
    RAINNBDAYS = 'RAINNBDAYS'

    def tr(self, string):
        """
        Returns a translatable string with the self.tr() function.
        """
        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        return ExampleProcessingAlgorithm()

    def name(self):
        """
        Returns the algorithm name, used for identifying the algorithm. This
        string should be fixed for the algorithm, and must not be localised.
        The name should be unique within each provider. Names should contain
        lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'TimeSeriesIndexTrend'

    def displayName(self):
        """
        Returns the translated algorithm name, which should be used for any
        user-visible display of the algorithm name.
        """
        return self.tr('TimeSeriesIndexTrend')

    def group(self):
        """
        Returns the name of the group this algorithm belongs to. This string
        should be localised.
        """
        return self.tr('TSNDVI')

    def groupId(self):
        """
        Returns the unique ID of the group this algorithm belongs to. This
        string should be fixed for the algorithm, and must not be localised.
        The group id should be unique within each provider. Group id should
        contain lowercase alphanumeric characters only and no spaces or other
        formatting characters.
        """
        return 'TSNDVI'

    def shortHelpString(self):
        """
        Returns a localised short helper string for the algorithm. This string
        should provide a basic description about what the algorithm does and the
        parameters and outputs associated with it..
        """
        return self.tr("Compute the trend of the vegetation index time series")

    def initAlgorithm(self, config=None):
        """
        Here we define the inputs and output of the algorithm, along
        with some other properties.
        """

        # We add the input vector features source. It can have any kind of
        # geometry.
        self.addParameter(
            QgsProcessingParameterFile(
                self.INPUT,
                self.tr('Input NDVI_time_series file')
            )
        )

        self.addParameter(
            QgsProcessingParameterFile(
                self.INPUTDATE,
                self.tr('Input NDVI dates file')
            )
        )
        
        self.addParameter(
            QgsProcessingParameterVectorLayer(
                self.CLIP,
                self.tr('Clip vector layer')
            )
        )
        
        self.addParameter(
            QgsProcessingParameterFolderDestination(
                self.RAINFALL,
                self.tr('Rainfall folder')
            )
        )

        self.addParameter(
            QgsProcessingParameterString(
                self.PREFIX,
                self.tr('Output files prefix'),
                ''
            )
        )
        self.addParameter(
            QgsProcessingParameterString(
                self.BEGDATE,
                self.tr('Beginning date for selection (YYYYMMDD)'),
                defaultValue='20000101'
            )
        )
        self.addParameter(
            QgsProcessingParameterString(
                self.ENDDATE,
                self.tr('End date for selection (YYYYMMDD)'),
                defaultValue=datetime.date.today().strftime("%Y%m%d")
            )
        )
        self.addParameter(
            QgsProcessingParameterEnum(
                self.REDUCE,
                self.tr('reduce method: cumul/max/amplitude'),
                options=['cumul','max','amplitude'],
                defaultValue='cumul'
                ))

        self.addParameter(
            QgsProcessingParameterNumber(
                self.CUMDAY,
                self.tr('Starting day for cumul method'),
                defaultValue=1,
                minValue=1,
                maxValue=31))
        self.addParameter(
            QgsProcessingParameterNumber(
                self.CUMMONTH,
                self.tr('Starting month for cumul method'),
                defaultValue=11,
                minValue=1,
                maxValue=12))
        self.addParameter(
            QgsProcessingParameterNumber(
                self.NBDAYS,
                self.tr('Number of days for cumulative period'),
                defaultValue=150
                ))
                
        self.addParameter(
            QgsProcessingParameterNumber(
                self.RAINDAY,
                self.tr('Starting day for cumul method (rainfall)'),
                defaultValue=1,
                minValue=1,
                maxValue=31))
        self.addParameter(
            QgsProcessingParameterNumber(
                self.RAINMONTH,
                self.tr('Starting month for cumul method (rainfall)'),
                defaultValue=11,
                minValue=1,
                maxValue=12))
        self.addParameter(
            QgsProcessingParameterNumber(
                self.RAINNBDAYS,
                self.tr('Number of days for cumulative period (rainfall)'),
                defaultValue=150
                ))

        self.addParameter(
            QgsProcessingParameterFolderDestination(
                self.OUTPUT,
                self.tr('Output folder')
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        """
        Here is where the processing itself takes place.
        """

        # Retrieve the feature source and sink. The 'dest_id' variable is used
        # to uniquely identify the feature sink, and must be included in the
        # dictionary returned by the processAlgorithm function.
        TS_file = self.parameterAsString(
            parameters,
            self.INPUT,
            context
        )
        
        datefile = self.parameterAsString(
            parameters,
            self.INPUTDATE,
            context
        )

        clip = self.parameterAsVectorLayer(
            parameters,
            self.CLIP,
            context
        )

        output_folder = self.parameterAsString(
            parameters,
            self.OUTPUT,
            context
        )

        prefix = parameters['PREFIX'] if parameters['PREFIX'] != None else ''
        if len(prefix)>0 and prefix[-1] != '_':
            prefix+='_'
        rain_folder = parameters['RAINFALL']
        begin_date = parameters['BEGDATE'] if parameters['BEGDATE'] != None else 20000101
        end_date = parameters['ENDDATE'] if parameters['ENDDATE'] != None else int(datetime.date.today().strftime("%Y%m%d"))
        reduceparams=['cumul','max','amplitude']
        reduce = reduceparams[parameters['REDUCE']]
        if reduce not in ['cumul','max','amplitude']:
            sys.exit("Reduce parameter has to be one of the following value : cumul/max/amplitude")
        elif reduce == 'cumul':
            cum_start_d = parameters['CUMDAY']
            cum_start_m = parameters['CUMMONTH']
            cum_nb_days = parameters['NBDAYS']
            rain_start_d = parameters['RAINDAY']
            rain_start_m = parameters['RAINMONTH']
            rain_nb_days = parameters['RAINNBDAYS']

        if os.path.exists(os.path.join(output_folder,'temp'))==False:
            os.mkdir(os.path.join(output_folder,'temp'))
        if int(begin_date)!=20000101 or int(end_date)!=int(datetime.date.today().strftime("%Y%m%d")):
            TS_file, datefile = NDVI_subdates_maker(TS_file,datefile,output_folder,int(begin_date),int(end_date),context,feedback)
        
        NDVI_TS =  QgsRasterLayer(TS_file)
        outputs={'ndvitrend': os.path.join(output_folder,'temp',prefix+'NDVI_trend.tif'),
                 'ndvilabel': os.path.join(output_folder,'temp',prefix+'NDVI_label.tif'),
                 'ndvicumul': os.path.join(output_folder,'temp',prefix+'NDVI_cumul.tif'),
                 'rainfcumul': os.path.join(output_folder,'temp',prefix+'rainfall_cumul.tif'),
                 'residues': os.path.join(output_folder,'temp',prefix+'NDVI_residues.tif'),
                 'restrend': os.path.join(output_folder,'temp',prefix+'NDVI_trend_res.tif'),
                 'reslabel': os.path.join(output_folder,'temp',prefix+'NDVI_label_res.tif'),
                 'factorslabel': os.path.join(output_folder,'temp',prefix+'factor_label.tif'),
                 'pearsoncoef': os.path.join(output_folder,'temp',prefix+'pearson_coef.tif')}
        TSIT_parameters={'ndvits':NDVI_TS,
                         'ndvidates':datefile,
                         'ndvi.reduce':reduce,
                         'ndvitrend': outputs['ndvitrend'],
                         'ndvilabel': outputs['ndvilabel']}
        if reduce =='cumul':
            TSIT_parameters['ndvi.reduce.cumul.startday']=cum_start_d
            TSIT_parameters['ndvi.reduce.cumul.startmonth']=cum_start_m
            TSIT_parameters['ndvi.reduce.cumul.nbdays']=cum_nb_days
            TSIT_parameters['ndvicumul']=outputs['ndvicumul']
        if rain_folder != None:
            rainfts = glob.glob(os.path.join(rain_folder,'*scrubbed*.nc'))
            rainfts.sort()
            rainfdates = rainfDates(rainfts,os.path.join(rain_folder,'rainf_dates.txt'))
            if begin_date!=None or end_date!=None:
                rainfts, rainfdates= subdates_maker(rainfts,rainfdates,output_folder,int(begin_date),int(end_date))
            if 'shapeMasked' in os.path.basename(rainfts[0]):
                rainfts = ['NETCDF:"' + x + '":GPM_3IMERGDE_06_precipitationCal' for x in rainfts]
            VRT_rf_file = os.path.join(output_folder,'temp',prefix+'input_rf.vrt')
            VRTOptions=gdal.BuildVRTOptions(separate=True)
            gdal.BuildVRT(VRT_rf_file,rainfts,options=VRTOptions)
            RF_TS_VRT =  QgsRasterLayer(VRT_rf_file)
            TSIT_parameters['rainfts']=RF_TS_VRT
            TSIT_parameters['rainfdates']=rainfdates
            TSIT_parameters['rain.interpolator']='linear'
            TSIT_parameters['residues']=outputs['residues']
            TSIT_parameters['restrend']=outputs['restrend']
            TSIT_parameters['reslabel']=outputs['reslabel']
            TSIT_parameters['factorslabel']=outputs['factorslabel']
            TSIT_parameters['pearsoncoef']=outputs['pearsoncoef']
            if reduce =='cumul':
                TSIT_parameters['rain.reduce']='cumul'
                TSIT_parameters['rain.reduce.cumul.startday']=rain_start_d
                TSIT_parameters['rain.reduce.cumul.startmonth']=rain_start_m
                TSIT_parameters['rain.reduce.cumul.nbdays']=rain_nb_days
                TSIT_parameters['rainfcumul']=outputs['rainfcumul']
        # To run another Processing algorithm as part of this algorithm, you can use
        # processing.run(...). Make sure you pass the current context and feedback
        # to processing.run to ensure that all temporary layer outputs are available
        # to the executed algorithm, and that the executed algorithm can send feedback
        # reports to the user (and correctly handle cancellation and progress reports!)
        processing.run("otb:TimeSeriesIndexTrend", TSIT_parameters, context=context, feedback=feedback)

        mask = os.path.join(output_folder, prefix+"mask.tif")
        processing.run("otb:Rasterization", {"in":clip,"im":outputs['ndvitrend'],"mode.binary.foreground":1,"out":mask, "outputpixeltype":0}, context=context, feedback=feedback)

        for key in outputs:
            raster = outputs[key]
            out = os.path.join(output_folder,os.path.basename(raster))
            expr=""
            ds = gdal.Open(raster)
            nb=ds.RasterCount+1
            for i in range(1,nb):
                expr+="(im2b1 == 0? -9999 :im1b{});".format(str(i))
            expr=expr[:-1]
            ds=None
            processing.run("otb:BandMathX", {'il':[raster,mask],'exp':expr,'out':out, 'outcontext':os.path.join(output_folder,prefix+"outcontext.txt")}, context=context, feedback=feedback)
            os.remove(os.path.join(output_folder,prefix+"outcontext.txt"))
            setNoDataValue(out,-9999)

        # Return the results of the algorithm. In this case our only result is
        # the feature sink which contains the processed features, but some
        # algorithms may return multiple feature sinks, calculated numeric
        # statistics, etc. These should all be included in the returned
        # dictionary, with keys matching the feature corresponding parameter
        # or output names.
        
        return {'OUTPUT':None}
