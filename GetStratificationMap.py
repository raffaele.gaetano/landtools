# -*- coding: utf-8 -*-

import warnings

from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (QgsProcessingAlgorithm,
                       QgsProcessingParameterNumber,
                       QgsProcessingParameterRasterLayer,
                       QgsProcessingParameterVectorDestination)
from qgis import processing
import os,sys

sys.path.append(os.path.dirname(__file__))
from landtools_utils import *

class GetStratificationMap(QgsProcessingAlgorithm):

    INPUT = 'INPUT'
    THRESHOLD = 'THRESHOLD'
    CW = 'CW'
    SW = 'SW'
    OUTPUT = 'OUTPUT'

    def tr(self, string):
        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        return GetStratificationMap()

    def name(self):
        return 'GetStratificationMap'

    def displayName(self):
        return self.tr('Get Stratification Map')

    def group(self):
        return self.tr('TSNDVI')

    def groupId(self):
        return 'TSNDVI'

    def shortHelpString(self):
        return self.tr("Segment a stratification metric to produce a map.")

    def initAlgorithm(self, config=None):

        self.addParameter(
            QgsProcessingParameterRasterLayer(
                self.INPUT,
                self.tr('Input landscape stratification metric')
            )
        )

        self.addParameter(
            QgsProcessingParameterNumber(
                self.THRESHOLD,
                self.tr('Threshold for Baatz and Schaepe criterion'),
                type=0,
                defaultValue=1000
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.CW,
                self.tr('Color (spectral) weight'),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=0.5
            )
        )
        self.addParameter(
            QgsProcessingParameterNumber(
                self.SW,
                self.tr('Spatial (compactness) weight'),
                type=QgsProcessingParameterNumber.Double,
                defaultValue=0.5
            )
        )

        self.addParameter(
            QgsProcessingParameterVectorDestination(
                self.OUTPUT,
                self.tr('Output landscape stratification metric')
            )
        )

    def processAlgorithm(self, parameters, context, feedback):

        metric_ly = self.parameterAsRasterLayer(
            parameters,
            self.INPUT,
            context
        )
        
        metric = metric_ly.dataProvider().dataSourceUri()

        output_vector = self.parameterAsOutputLayer(
            parameters,
            self.OUTPUT,
            context
        )

        ndv = getNoDataValue(metric)
        output_folder = os.path.dirname(output_vector)

        threshold = parameters['THRESHOLD']
        cw = parameters['CW']
        sw = parameters['SW']

        prefix = os.path.splitext(os.path.basename(output_vector))[0] + '_'
        tmp_name = prefix + 'temp'
        tmp_folder = os.path.join(output_folder, tmp_name)
        if not os.path.exists(tmp_folder):
            os.mkdir(tmp_folder)

        to_del = []

        grm_pre = os.path.join(tmp_folder, prefix + 'segraw.tif')
        grm_params = {'in': metric, 'out': grm_pre, 'criterion': 'bs', 'criterion.bs.cw': cw, 'criterion.bs.sw': sw, 'threshold': threshold}
        processing.run('otb:LSGRM', grm_params, context=context, feedback=feedback)
        to_del.append(grm_pre)

        ndt = os.path.join(tmp_folder, prefix + '_segndt.tif')
        ndt_params = {'il': [metric, grm_pre], 'exp': 'im1b1 != ' + str(ndv) + ' ? im2b1 : 0', 'out': ndt}
        processing.run('otb:BandMath', ndt_params, context=context, feedback=feedback)
        to_del.append(ndt)

        vec = output_vector
        vec_params = {'in': ndt, 'out': vec}
        out = processing.run('otb:SimpleVectorization', vec_params, context=context, feedback=feedback)

        for x in to_del:
            try:
                os.remove(x)
            except:
                warnings.warn('Could not remove ' + x)
        if len(os.listdir(tmp_folder)) == 0:
            os.rmdir(tmp_folder)

        return {self.OUTPUT: out}
