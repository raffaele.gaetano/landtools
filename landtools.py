#try:
from osgeo import gdal, ogr
#except:
#    import gdal, ogr

import datetime
import otbApplication as otb
import os
from landtools_utils import *
import shutil

def temporal_smoothing(file_list, output_file):
    nd = len(file_list)
    ds = gdal.Open(file_list[0])
    ndv = ds.GetRasterBand(1).GetNoDataValue()
    ds = None

    dates = [x.split('_')[-2][3:] for x in file_list]
    sdates = [datetime.datetime.strptime(d, '%Y%j').date().strftime('%Y%m%d') for d in dates]
    datefile = os.path.splitext(output_file)[0] + '_dates.txt'

    with open(datefile, 'w') as df:
        df.write('\n'.join(sdates) + '\n')

    sg = otb.Registry.CreateApplication('TemporalSmoothing')
    sg.SetParameterStringList('il', file_list)
    sg.SetParameterString('dates', datefile)
    sg.Execute()

    msks = otb.Registry.CreateApplication('BandMathX')
    msks.AddImageToParameterInputImageList('il', sg.GetParameterOutputImage('out'))
    msks.SetParameterString('exp', '{' + ';'.join(['im1b%d == ' % i + str(ndv) for i in range(1, nd+1)]) + '}')
    msks.Execute()

    vmsk = otb.Registry.CreateApplication('BandMath')
    vmsk.AddImageToParameterInputImageList('il', msks.GetParameterOutputImage('out'))
    vmsk.SetParameterString('exp', '+'.join(['im1b%d' % i for i in range(1,nd+1)]) + '!=' + str(nd))
    vmsk.Execute()

    cmsk = otb.Registry.CreateApplication('BandMathX')
    cmsk.AddImageToParameterInputImageList('il', msks.GetParameterOutputImage('out'))
    cmsk.AddImageToParameterInputImageList('il', vmsk.GetParameterOutputImage('out'))
    cmsk.SetParameterString('exp', '{' + ';'.join(['im1b%d*im2b1' % i for i in range(1,nd+1)]) + '}')
    cmsk.Execute()

    gf = otb.Registry.CreateApplication('ImageTimeSeriesGapFilling')
    gf.SetParameterInputImage('in', sg.GetParameterOutputImage('out'))
    gf.SetParameterInputImage('mask', cmsk.GetParameterOutputImage('out'))
    gf.SetParameterInt('comp', 1)
    gf.SetParameterString('it', 'linear')
    gf.SetParameterString('out', output_file)
    gf.SetParameterOutputImagePixelType('out', otb.ImagePixelType_int16)
    gf.ExecuteAndWriteOutput()

    ds = gdal.Open(output_file, gdal.GA_Update)
    ds.GetRasterBand(1).SetNoDataValue(ndv)
    ds = None

    return

def landscape_stratification_metric(fn, output_file, md=[1, 1], duration=365, cbegin=2, cend=5, date_file=None):
    if date_file is None:
        date_file = os.path.splitext(fn)[0] + '_dates.txt'

    ds = gdal.Open(fn)
    ndv = ds.GetRasterBand(1).GetNoDataValue()
    ds = None

    msk = otb.Registry.CreateApplication('ManageNoData')
    msk.SetParameterString('in', fn)
    msk.SetParameterString('mode', 'buildmask')
    msk.Execute()

    periods = get_period_intervals(date_file, md, duration)

    mts = otb.Registry.CreateApplication('BandMathX')
    mts.SetParameterStringList('il', [fn])
    mts.SetParameterString('exp', get_mean_expression(periods))
    mts.Execute()

    pca = otb.Registry.CreateApplication('LandscapeStratificationMetric')
    pca.AddImageToParameterInputImageList('ndvits', mts.GetParameterOutputImage('out'))
    pca.SetParameterInt('cbegin', cbegin)
    pca.SetParameterInt('cend', cend)
    pca.SetParameterFloat('bv', ndv)
    pca.SetParameterString('rescale', 'minmax')
    pca.SetParameterInt('rescale.minmax.outmin', 0)
    pca.SetParameterInt('rescale.minmax.outmax', 2048)
    pca.Execute()

    """
    dyn = otb.Registry.CreateApplication('DynamicConvert')
    dyn.SetParameterInputImage('in', pca.GetParameterOutputImage('out'))
    dyn.SetParameterInt('outmin', 1)
    dyn.SetParameterInt('outmax', 2048)
    dyn.SetParameterInt('quantile.low', 0)
    dyn.SetParameterInt('quantile.high', 0)
    dyn.EnableParameter('mask')
    dyn.SetParameterInputImage('mask', msk.GetParameterOutputImage('out'))
    dyn.SetParameterOutputImagePixelType('out', otb.ImagePixelType_int16)
    dyn.Execute()
    """

    fin = otb.Registry.CreateApplication('ManageNoData')
    fin.SetParameterInputImage('in', pca.GetParameterOutputImage('out'))
    fin.SetParameterString('mode', 'apply')
    fin.SetParameterInputImage('mode.apply.mask', msk.GetParameterOutputImage('out'))
    fin.SetParameterString('out', output_file)
    fin.SetParameterOutputImagePixelType('out', otb.ImagePixelType_int16)
    fin.ExecuteAndWriteOutput()

    return

def compute_stratification(metric_fn, th, output_file, cw = 0.5, sw = 0.5):
    ds = gdal.Open(metric_fn)
    ndv = ds.GetRasterBand(1).GetNoDataValue()
    ds = None

    seg = otb.Registry.CreateApplication('LSGRM')
    seg.SetParameterString('in', metric_fn)
    seg.SetParameterString('criterion', 'bs')
    seg.SetParameterFloat('criterion.bs.cw', cw)
    seg.SetParameterFloat('criterion.bs.sw', sw)
    seg.SetParameterFloat('threshold', th)
    seg.Execute()

    ndt = otb.Registry.CreateApplication('BandMath')
    ndt.SetParameterStringList('il', [metric_fn])
    ndt.AddImageToParameterInputImageList('il', seg.GetParameterOutputImage('out'))
    ndt.SetParameterString('exp', 'im1b1 != ' + str(ndv) + ' ? im2b1 : 0')
    ndt.Execute()

    fin = otb.Registry.CreateApplication('SimpleVectorization')
    fin.SetParameterInputImage('in', ndt.GetParameterOutputImage('out'))
    fin.SetParameterString('out', output_file)
    fin.ExecuteAndWriteOutput()

    return

def compute_ndvi_index_trends(input_ts, datefile, begin_date, end_date, output_folder,
                             ndvi_reduce='cumul', ndvi_start_day=1, ndvi_start_month=1, ndvi_nbdays=150,
                             rainfall_folder=None, rain_reduce='cumul', rain_start_day=1, rain_start_month=1, rain_nbdays=150, rain_interp='linear'):
    outdatefile = os.path.splitext(datefile)[0] + '_subperiod.txt'
    exp = get_subperiod_expression(datefile, begin_date, end_date, outdatefile)
    sp = otb.Registry.CreateApplication('BandMathX')
    sp.SetParameterStringList("il", [input_ts])
    sp.SetParameterString("exp", exp)
    sp.Execute()

    tr = otb.Registry.CreateApplication('TimeSeriesIndexTrend')
    tr.AddImageToParameterInputImageList('ndvits', sp.GetParameterOutputImage("out"))
    tr.SetParameterString('ndvidates', outdatefile)
    if rainfall_folder is not None:
        rainfall_dates = os.path.join(rainfall_folder, 'rainfall,query_dates.txt')
        rainfall_ts = get_rainfall_subperiod(rainfall_folder, begin_date, end_date, rainfall_dates)
        tr.SetParameterStringList("rainfts", rainfall_ts)
        tr.SetParameterString("rainfdates", rainfall_dates)
    tr.SetParameterString('ndvi.reduce', ndvi_reduce)
    if ndvi_reduce == 'cumul':
        tr.SetParameterInt('ndvi.reduce.cumul.startday', ndvi_start_day)
        tr.SetParameterInt('ndvi.reduce.cumul.startmonth', ndvi_start_month)
        tr.SetParameterInt('ndvi.reduce.cumul.nbdays', ndvi_nbdays)
    if rainfall_folder is not None:
        tr.SetParameterString('rain.reduce', rain_reduce)
        if rain_reduce == 'cumul':
            tr.SetParameterInt('rain.reduce.cumul.startday', rain_start_day)
            tr.SetParameterInt('rain.reduce.cumul.startmonth', rain_start_month)
            tr.SetParameterInt('rain.reduce.cumul.nbdays', rain_nbdays)
        tr.SetParameterString('rain.interpolator', rain_interp)
    prefix = os.path.splitext(os.path.basename(input_ts))[0]
    outputs={'ndvitrend': os.path.join(output_folder, prefix+'_NDVI_trend.tif'),
             'ndvilabel': os.path.join(output_folder, prefix+'_NDVI_label.tif'),
             'ndvicumul': os.path.join(output_folder, prefix+'_NDVI_cumul.tif')}
    if rainfall_folder is not None:
        outputs['pearsoncoef'] = os.path.join(output_folder, prefix+'_pearson_coef.tif')
        outputs['rainfcumul'] = os.path.join(output_folder, prefix+'_rainfall_cumul.tif')
        outputs['residues'] = os.path.join(output_folder, prefix+'_NDVI_residues.tif')
        outputs['restrend'] = os.path.join(output_folder, prefix+'_NDVI_trend_res.tif')
        outputs['reslabel'] = os.path.join(output_folder, prefix+'_NDVI_label_res.tif')
        outputs['factorslabel'] = os.path.join(output_folder, prefix+'_factor_label.tif')
    print(outputs)
    for k,v in outputs.items():
        tr.SetParameterString(k, v)
    os.makedirs(output_folder, exist_ok=True)
    tr.ExecuteAndWriteOutput()

    for o in ['ndvilabel', 'reslabel']:
        with gdal.Open(outputs[o], gdal.GA_Update) as ds:
            ds.GetRasterBand(1).SetNoDataValue(0)
        shutil.copyfile('./aux/ndvi_trends_style.qml', os.path.splitext(outputs[o])[0]+'.qml')

    return
