QGIS processing scripts for CIRAD-TETIS Land Monitoring tools.

Currently contains :

- TemporalSmoothing, an app for Savitzky-Golay filtering of time series.
- LandscapeStratification, an app for land units segmentation based on multiannual NDVI time series analysis.
- TimeSeriesIndexTrends, an app for mapping vegetation trends using multiannual NDVI time series.

Needs :

QGIS version >= 3.14 (or any 3.x version supporting Orfeo Toolbox as a provider)

A specific OTB build (ask the developers for details)

For non-expert users: 

*Linux* copy script in $HOME/.local/share/QGIS/QGIS3/profiles/default/processing/scripts

*Windows* copy script in %AppData%/QGIS/QGIS3/profiles/default/processing/scripts
